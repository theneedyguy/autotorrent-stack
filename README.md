Autotorrent TV Show Stack
=======

A Docker stack to automatically download TV series via QBittorrent Webclient using your [showrss.info](http://showrss.info) feed.

__What it does__:
This stack is made for people who run their own server that they use as a media center. It aims to automate the process of downloading the latest torrents of your favourite shows. I personally run it on my NAS :)

__What it does NOT__:
This stack will not provide you with a media center. For this you should use for instance [emby.media](http://emby.media) or [plex.tv](http://plex.tv).

Architecture
------------

The stack comes in 2 parts.

- __qBittorrent container__ that will download Torrents (Magnet Links) that it receives via its HTTP API. Is will be reachable on `http://<your-server-ip>:9999` after bringing up the stack.
- __Showrss container__ that checks your feed for new shows and sends the Magnet Links to the qBittorrent container via HTTP API.

The qbittorrent container is an already available container made by linuxserver.io that provides you with the qbittorrent WebUI.

The showrss container is made by me and contains a Python script that executes all the logic regarding showrss.

![Architecure](./img/arch.png)

Requirements
------------

- Docker (__Required__)
- Docker-Compose (__Optional__ but recommended since it simplifies the whole setup)
- [showrss.info](http://showrss.info) account with some TV shows added to your feed. (__Required__)

Installation
------------

1. Clone the git repo to your system.

```bash
git clone https://gitlab.com/theneedyguy/autotorrent-stack.git
```

2. After [configuring](#configuration) the docker-compose file start the stack.

```bash
docker-compose up -d
```

Running the stack will create folders for qbittorrent where it saves some configuration and also the downloaded files to. You can change the paths manually by editing the docker-compose.yml file in the repo. __Remember to change the qBittorrent password in the webui and in the docker-compose file! `http://<your-server-ip>:9999`__

Configuration
-------------

The docker-compose.yml file containes 2 services that can be configured via environment variables. You should at least have some knowledge on how the docker-compose file works.

__The docker-compose.yml file:__

```yaml
version: '3.3'

services:
  showrss:
    image: ckevi/showrss
    container_name: showrss
    restart: unless-stopped
    depends_on:
      - qbittorrent
    environment:
      - USER_ID=10000
      - TORRENT_QUALITY=default
      - QBITTORRENT_USER=admin
      - QBITTORRENT_PASS=adminadmin
      - QBITTORRENT_HOST=http://qbittorrent:9999

  qbittorrent:
    image: linuxserver/qbittorrent
    container_name: qbittorrent
    restart: unless-stopped
    environment:
      - WEBUI_PORT=9999
      - PUID=0
      - PGID=0
    volumes:
      - "./qconfig:/config"
      - "./qdownloads:/downloads"
    ports:
      - "9999:9999"
      - "6881:6881"
      - "6881:6881/udp"

```

### Showrss Container Variables

| Environment variable | Default value | Example value           | Purpose                                                                                  |
|----------------------|---------------|-------------------------|------------------------------------------------------------------------------------------|
| USER_ID              | (empty)       | 10000                   | The required user id of your showrss account (Can be found at http://showrss.info/feeds) |
| TORRENT_QUALITY      | default       | default                 | Defines the quality of the torrent that will be downloaded.                              |
| QBITTORRENT_USER     | (empty)       | admin                   | The user of the qbittorrent webui (used in API calls)                                    |
| QBITTORRENT_PASS     | (empty)       | adminadmin              | The password of the qbittorrent webui (used in API calls)                                |
| QBITTORRENT_HOST     | (empty)       | http://qbittorrent:9999 | The url to your qbittorrent host / container                                             |

Getting a user ID
-----------------

The user ID is your Showrss user identifier.

You can find this by clicking on the 'feeds' page on [showrss.info](http://showrss.info) and generating a personal feed. The URL for the feed will be something like:

    http://showrss.info/user/10000.rss?magnets=true&namespaces=true&name=null&quality=null&re=null

Your user ID is the value after `/user/` and before `.rss`. So it is `10000` in this example

Changelog
---------

## Version 1.2

- Automatically replace spaces in show titles with '+'
- Added deletejob container that removed completed torrents. (Files persist)

## Version 1.1

- Changed the way files get saved in qBittorrent. Now all episodes get saved in a folder with the name of the TV Show. This is to help Emby or Plex recognize the video files and index them in the library.

## Version 1.0

- Initial release.
