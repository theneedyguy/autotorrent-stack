FROM python:3.6-slim
RUN mkdir -p /opt/showrss

COPY ./showrss /opt/showrss/
COPY requirements.txt /opt/showrss/

RUN pip install -r /opt/showrss/requirements.txt

#USER nobody
WORKDIR /opt/showrss

ENTRYPOINT ["python3", "showrss"]
