#!/usr/bin/env python

import configparser
import hashlib
import syslog
import logging
import os.path
import os
import requests
import time

import feedparser

logging.basicConfig(format='[%(asctime)s]-%(levelname)s -- %(message)s', level=logging.DEBUG)

def log(message):
    logging.info(message)


def addTorrent(username, password, qbittorrent_host, magnetlink, foldername="dl_no_name"):
    session = requests.session()
    foldername = foldername.replace(" ", "+")
    try:
        session.post(qbittorrent_host + "/api/v2/auth/login", data={"username":username, "password":password})
        log("Authenticated with Msg: '%s'" % str(session))
        session.post(qbittorrent_host + "/api/v2/torrents/add", data={"urls":magnetlink, "savepath":"/downloads/"+foldername, "root_folder":False})
    except Exception as e:
        log("Error sending request: '%s'" % str(e))


# Settings.
user_id = os.environ["USER_ID"]
quality = "default"

try:
    quality = os.environ["TORRENT_QUALITY"]
except:
    pass
if not quality in ["default", "standard", "high", "all"]:
    exit("Unknown quality setting '%s'.\nMust be one of 'default', 'standard', 'high' or 'all'." % quality)


# Convert the quality setting to the correct parameter.
hd = { "default": "null", "standard": "sd", "high": "hd", "all": "any" }[quality]

# Generate the URL.
url = "http://showrss.info/user/%s.rss?magnets=true&namespaces=true&name=clean&quality=%s&re=null" % (user_id, hd)


def checkShows(showRSSUrl):
    # Logging.
    log("Checking for new shows")
    # Load cache
    cachefile = os.path.expanduser(".showrss_cache")
    cache = configparser.ConfigParser()
    cache.read(cachefile)
    # Fetch the feed.
    feed = feedparser.parse(url)

    # Ensure there's a section in the cache.
    try:
        cache.add_section("entries")
    except:
        pass
    
    shows = []

    for item in feed['items']:

        title = item['title']
        identifier = item['id']
        key = hashlib.md5(identifier.encode("ascii")).hexdigest()
        torrent = item['links'][0]['href']
        show_name = item["tv_show_name"]
        
        try:
            cache.get("entries", key)
        except:
            log("Downloading '%s'..." % title)
            try:
                cache.set("entries", key, "1")
                addTorrent(os.environ["QBITTORRENT_USER"], os.environ["QBITTORRENT_PASS"], os.environ["QBITTORRENT_HOST"], torrent, show_name)
                with open(cachefile, 'w+') as outputfile:
                    cache.write(outputfile)
                    outputfile.close()
                    shows.append(title)
            except Exception as e:
                log("Error: '%s'" % str(e))
                log("Unable to download '%s'" % torrent)

        
# Wait for first execution in case the container stack is not up.
log("Waiting 20 seconds for stack")
time.sleep(20)
while (True):
    # Run indefinitely
    checkShows(url)
    log("Waiting 1h for next check")
    # Wait 1 hour for next check
    time.sleep(3600)

